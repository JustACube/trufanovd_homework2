<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 08.07.18
 * Time: 12:38
 */

interface flyInterface
{
    public function doFly();
    public function getName(): string ;
}