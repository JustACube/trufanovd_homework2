<?php

/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 08.07.18
 * Time: 15:52
 * @param flyInterface $fly
 */
require_once ("/home/dmitry/PhpstormProjects/hm2/Bird.php");
require_once ("/home/dmitry/PhpstormProjects/hm2/Airplane.php");
require_once ("/home/dmitry/PhpstormProjects/hm2/PaperAirplane.php");
require_once ("/home/dmitry/PhpstormProjects/hm2/flyInterface.php");

function setFlying(flyInterface $fly){
    $fly->doFly();
    echo $fly->getName(). " sent to sky\n";
}



$bird = new Bird();
$airplane = new Airplane();
$paper_airplane = new PaperAirplane();


setFlying($bird);
setFlying($airplane);
setFlying($paper_airplane);

