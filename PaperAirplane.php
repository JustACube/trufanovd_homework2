<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 08.07.18
 * Time: 12:44
 */

require_once("/home/dmitry/PhpstormProjects/hm2/flyInterface.php");

class PaperAirplane implements flyInterface
{
    private $startFlyingAsPaperAirplane;
    private $name;

    /**
     * PaperAirplane constructor.
     */
    public function __construct()
    {
        $this->startFlyingAsPaperAirplane=false;
        $this->name=get_class($this);
    }

    /**
     * @return bool
     */
    public function isStartFlyingAsPaperAirplane()
    {
        return $this->startFlyingAsPaperAirplane;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function doFly()
    {
        $this->startFlyingAsPaperAirplane=true;
    }
}