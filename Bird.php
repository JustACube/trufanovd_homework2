<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 08.07.18
 * Time: 12:42
 */
require_once("/home/dmitry/PhpstormProjects/hm2/flyInterface.php");

class Bird implements flyInterface
{
    private $startFlyingAsBird;
    private $name;

    /**
     * Bird constructor.
     */
    public function __construct()
    {
        $this->startFlyingAsBird=false;
        $this->name=get_class($this);
    }

    /**
     * @return bool
     */
    public function isStartFlyingAsBird()
    {
        return $this->startFlyingAsBird;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function doFly()
    {
        $this->startFlyingAsBird=true;
    }
}