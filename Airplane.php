<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 08.07.18
 * Time: 12:41
 */

require_once("/home/dmitry/PhpstormProjects/hm2/flyInterface.php");

class Airplane implements flyInterface
{
    private $startFlyingAsAirplane;
    private $name;

    /**
     * Airplane constructor.
     */
    public function __construct()
    {
        $this->startFlyingAsAirplane=false;
        $this->name=get_class($this);
    }

    /**
     * @return bool
     */
    public function isStartFlyingAsAirplane()
    {
        return $this->startFlyingAsAirplane;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function doFly()
    {
        $this->startFlyingAsAirplane=true;
    }
}